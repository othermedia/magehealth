<?php
/**
 * @copyright   the OTHER media, 26 February 2013
 * @package     OtherMedia_MageHealth
 * @author      Team Magento <magento@othermedia.com>
 */


class OtherMedia_MageHealth_Model_Notification_Pushover extends Varien_Object
{

    const XML_PATH_PUSHOVER_ENABLED = 'othermedia_magehealth/pushover/enabled';
    const XML_PATH_PUSHOVER_ENDPOINT = 'othermedia_magehealth/pushover/endpoint';
    const XML_PATH_PUSHOVER_APPLICATION_TOKEN = 'othermedia_magehealth/pushover/application_token';
    const XML_PATH_PUSHOVER_USER_TOKEN = 'othermedia_magehealth/pushover/user_token';
    const XML_PATH_PUSHOVER_APPLICATION_TITLE = 'othermedia_magehealth/pushover/application_title';
    const XML_PATH_PUSHOVER_DEFAULT_MESSAGE = 'othermedia_magehealth/pushover/default_message';

    /*
     * *
     * Send a push notification if the alert is over the threshold
     *
     */
    public function notify($title, $status, $threshold, $current)
    {
        if (Mage::getStoreConfigFlag(self::XML_PATH_PUSHOVER_ENABLED))
        {
            $message = sprintf(Mage::getStoreConfig(self::XML_PATH_PUSHOVER_DEFAULT_MESSAGE), $title, $status, $threshold, $current);        
            curl_setopt_array($ch = curl_init(), array(
                CURLOPT_URL => Mage::getStoreConfig(self::XML_PATH_PUSHOVER_ENDPOINT),
                CURLOPT_POSTFIELDS => array(
                    "token" => Mage::getStoreConfig(self::XML_PATH_PUSHOVER_APPLICATION_TOKEN),
                    "user" => Mage::getStoreConfig(self::XML_PATH_PUSHOVER_USER_TOKEN),
                    "title" => Mage::getStoreConfig(self::XML_PATH_PUSHOVER_APPLICATION_TITLE),
                    "message" => $message
                ),
                CURLOPT_RETURNTRANSFER => true
            )); 
            curl_exec($ch);
            curl_close($ch);
        }
    }
    
}