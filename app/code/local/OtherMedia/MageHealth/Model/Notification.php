<?php
/**
 * @copyright   the OTHER media, 26 February 2013
 * @package     OtherMedia_MageHealth
 * @author      Team Magento <magento@othermedia.com>
 */


class OtherMedia_MageHealth_Model_Notification extends Mage_Core_Model_Abstract
{

     /*
     * *
     * Trigger notifications
     *
     */
    public function notify($title, $status, $threshold, $current)
    {
        Mage::getModel('othermedia_magehealth/notification_pushover')->notify($title, $status, $threshold, $current);
    }

}