<?php
/**
 * @copyright   the OTHER media, 26 February 2013
 * @package     OtherMedia_MageHealth
 * @author      Team Magento <magento@othermedia.com>
 */


class OtherMedia_MageHealth_Model_Orders extends Mage_Core_Model_Abstract
{

    /**
     *
     * Retrieve time of the last order for store id supplied
     * We are retrieving from sales_flat_order rather than the custom CIMS tables
     * @param integer $store_id
     * @param boolean $relative If set to yes relative time string will be returned
     */
    public function getLastOrderTime($store_id, $relative = false)
    {
        $collection = Mage::getModel('sales/order')
                            ->getCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('store_id', $store_id)
                            ->setOrder('created_at', 'DESC')
                            ->setPage(1, 1)
                            ->load();
        $lastOrderTimeStamp = strtotime($collection->getFirstItem()->getCreatedAt());
        $return = Mage::helper('othermedia_magehealth')->getTimeElapsed($lastOrderTimeStamp, 2, $relative);
        return $return;
    }
    
}