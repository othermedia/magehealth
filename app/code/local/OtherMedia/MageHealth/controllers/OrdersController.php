<?php
/**
 * @copyright   the OTHER media, 26 February 2013
 * @package     OtherMedia_MageHealth
 * @author      Team Magento <magento@othermedia.com>
 */
 
class OtherMedia_MageHealth_OrdersController extends OtherMedia_MageHealth_Controller_Abstract
{

    /**
     * Get orders model
     */
    protected function _getOrdersModel()
    {
        return Mage::getModel('othermedia_magehealth/orders');
    }
    
    /**
     * 
     * Main method to return time from last order
     * @return string 
     * 
     * Example:
     * /magehealth/orders/lastorder/store/1/threshold/1000/json/0/relative/1
     * relative - 1 - time in relative format, otherwise in seconds
     * json - json format
     * 
     */
    public function lastorderAction()
    {
        $checkName = "orders_lastorder";
        $jsonFormat = $this->getRequest()->getParam('json');
        $relative = $this->getRequest()->getParam('relative', false);
        $outputFormat = $this->getRequest()->getParam('output');
        $storeId = $this->getRequest()->getParam('store', Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId());
        $alertThreshold = $this->getRequest()->getParam('threshold');
        $lastOrderRelative = false;
        if ($relative) {
            $lastOrderRelative = $this->_getOrdersModel()->getLastOrderTime($storeId, true);
        }    
        $lastOrder = $this->_getOrdersModel()->getLastOrderTime($storeId);
        $thresholdStatus = $this->_getHelper()->getIntThresholdStatus($alertThreshold, $lastOrder);
        $nagiosStatus = $this->_getHelper()->getIntNagiosStatus($alertThreshold, $lastOrder);
        
        if ($thresholdStatus == $this->_getHelper()->getStatusNotOk()){
            Mage::$this->_getNotificationModel()->notify($checkName, $thresholdStatus, $alertThreshold, $lastOrderRelative ? $lastOrderRelative : $lastOrder);
        }
        
        echo $this->getLayout()->createBlock('othermedia_magehealth/output')
                                ->setFormat($outputFormat)
                                ->setStatus($thresholdStatus)
                                ->setNagiosStatus($nagiosStatus)
                                ->setResponse($lastOrderRelative ? $lastOrderRelative : $lastOrder)
                                ->setCheckName($checkName)
                                ->setJson($jsonFormat)
                                ->toHtml();
    }
    
}