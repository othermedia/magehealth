<?php
/**
 * @copyright   the OTHER media, 26 February 2013
 * @package     OtherMedia_MageHealth
 * @author      Team Magento <magento@othermedia.com>
 */
 
class OtherMedia_MageHealth_Controller_Abstract extends Mage_Core_Controller_Front_Action
{
    
    /**
     * Get helper instance
     *
     * @return OtherMedia_MageHealth_Helper_Data
     */
    protected function _getHelper() 
    {
        return  Mage::helper('othermedia_magehealth');
    }
    
    /**
     * Get notification model
     *
     * @return  OtherMedia_MageHealth_Model_Notification
     * @author  Team Magento <magento@othermedia.com>
     */
    protected function _getNotificationModel()
    {
        return Mage::getModel('othermedia_magehealth/notification');
    }
    
    /**
     * Check if module is enabled
     *
     * @author  Team Magento <magento@othermedia.com>
     */
    public function preDispatch()
    {
        parent::preDispatch();
        
        if (!$this->_getHelper()->getIsEnabled()) {
            $this->norouteAction();
            return $this;
        }
    }
    
} 