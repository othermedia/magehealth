<?php
/**
 * @copyright   the OTHER media, 26 February 2013
 * @package     OtherMedia_MageHealth
 * @author      Team Magento <magento@othermedia.com>
 */
 
class OtherMedia_MageHealth_Block_Output extends Mage_Core_Block_Abstract
{

    /**
     *
     * Write to expected output format and optionally serve as Json response
     *
     */
    protected function _toHtml()
    {
        switch ($this->getFormat()) {

            case 'pingdom':
                $response = '<pingdom_http_custom_check>' . PHP_EOL;
                $response .= '<status>' . $this->getStatus() . '</status>' . PHP_EOL;
                $response .= '<response_time>' . $this->getResponse() . '</response_time>' . PHP_EOL;
                $response .= '</pingdom_http_custom_check>';
                break;
                
            case 'nagios':
                return $this->getNagiosStatus();
                break;
                
            case 'gecko_number':
                $response = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
                $response .= '<root>' . PHP_EOL;
                $response .= '<item check="' . $this->getCheckName() . '">' . PHP_EOL;
                $response .= '<value>' . $this->getResponse() . '</value>' . PHP_EOL;
                $response .= '<text></text>' . PHP_EOL;
                $response .= '</item>' . PHP_EOL;
                $response .= '</root>' . PHP_EOL;
                break;
                
            default:
                $response = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
                $response .= '<othermedia_magehealth check="' . $this->getCheckName() . '">' . PHP_EOL;
                $response .= '<status>' . $this->getStatus() . '</status>' . PHP_EOL;
                $response .= '<response>' . $this->getResponse() . '</response>' . PHP_EOL;
                $response .= '</othermedia_magehealth>';
                break;
        }
        
        if ($this->getJson()) {
            return json_encode(simplexml_load_string($response));
        } else {
            return $response;
        }
        
    }

}