<?php
/**
 * @copyright   the OTHER media, 26 February 2013
 * @package     OtherMedia_MageHealth
 * @author      Team Magento <magento@othermedia.com>
 */

/**
 * Data helper
 *
 * @package     OtherMedia_MageHealth
 * @author      Team Magento <magento@othermedia.com>
 */
class OtherMedia_MageHealth_Helper_Data extends Mage_Core_Helper_Abstract
{

    const XML_PATH_ENABLED = 'othermedia_magehealth/general/enabled';
    const STATUS_NO_THRESHOLD = 'NO THRESHOLD SET';
    const STATUS_OK = 'OK';
    const STATUS_NOT_OK = 'NOT OK';
    const STATUS_NAGIOS_ERROR = 3;
    const STATUS_NAGIOS_DOWN = 2;
    const STATUS_NAGIOS_UP = 0;

    /**
     * Get is enabled
     *
     * @param   int     $storeId
     * @return  bool
     * @author  Marcell Kiss-Toth <marcell.kiss-toth@othermedia.com>
     */
    public function getIsEnabled($storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }

        return Mage::getStoreConfig(self::XML_PATH_ENABLED, $storeId);
    }
    
    /*
     * *
     * Get No Threshold status
     */
    public function getStatusNoThreshold($storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }
        return self::STATUS_NO_THRESHOLD;
    }
    
    /*
     * *
     * Get  OK status
     */
    public function getStatusOk($storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }
        return self::STATUS_OK;
    }
    
    /*
     * *
     * Get not OK status
     */
    public function getStatusNotOk($storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }
        return self::STATUS_NOT_OK;
    }
    
    /*
     * *
     * Get Nagios error status
     */
    public function getStatusNagiosError($storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }
        return self::STATUS_NAGIOS_ERROR;
    }
    
    /*
     * *
     * Get Nagios down status
     */
    public function getStatusNagiosDown($storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }
        return self::STATUS_NAGIOS_DOWN;
    }
    
    /*
     * *
     * Get Nagios up status
     */
    public function getStatusNagiosUp($storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }
        return self::STATUS_NAGIOS_UP;
    }
    
    /*
     * *
     * Get Int Threshold Status
     */
    public function getIntThresholdStatus($threshold = 0, $value)
    {
        if (is_null($threshold)){
            return self::STATUS_NO_THRESHOLD;
        } else {
            if ($threshold >= $value){
                return self::STATUS_OK;
            } else {
                return self::STATUS_NOT_OK;
            }
        }
    }
    
    /*
     * *
     * Return a Nagios-compatible int response for a check based on response time and threshold
     *
     */
    public function getIntNagiosStatus($threshold = null, $value)
    {
        if (is_null($threshold)){
            return self::STATUS_NAGIOS_ERROR;
        } else {
            if ($threshold < $value){
                return self::STATUS_NAGIOS_UP;
            } else {
                return self::STATUS_NAGIOS_DOWN;
            }
        }
    }
    
    /**
     * Converts time to relative string
     * @param float $ptime
     * @return string
     */
    public function getTimeElapsed(float $ptime, $detailLevel = 1, $relative = true) {
        $difference = Mage::getModel('core/date')->timestamp(time()) - $ptime;
        if (!$relative) {
            return $difference;
        }
        if ($difference < 1) {
            return '0 seconds';
        }

    	$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    	$lengths = array("60", "60", "24", "7", "4.35", "12", "10");
    
    	$remainders = array();
    
    	for($j = 0; $j < count($lengths); $j++) {
    		$remainders[$j] = floor(fmod($difference, $lengths[$j]));
    		$difference = floor($difference / $lengths[$j]);
    	}
    
    	$difference = round($difference);
    
    	$remainders[] = $difference;
    
    	$string = "";
    
    	for ($i = count($remainders) - 1; $i >= 0; $i--) {
    		if ($remainders[$i]) {
    			$string .= $remainders[$i] . " " . $periods[$i];
    
    			if($remainders[$i] != 1) {
    				$string .= "s";
    			}
    
    			$string .= " ";
    
    			$detailLevel--;
    
    			if ($detailLevel <= 0) {
    				break;
    			}
    		}
    	}
    
    	return $string . $tense;
        
    }

}
