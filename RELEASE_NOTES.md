0.1.0
=====

First release: MageHealth module to supply site specific information for monitoring & notification purposes

Available checks:

Orders
------

/magehealth/orders/lastorder/

Available params:

	store		a valid store id
	threshold	value in seconds
	output		pingdom / gecko_number / nagios / default =  tOm data structure
	json	    1 / 0 to return formatted as json instead of xml
	

CIMS
----

/magehealth/cims/unsynced/

Available params:

	threshold	value in seconds
	output		pingdom / gecko_number / nagios / default =  tOm data structure
	json	    1 / 0 to return formatted as json instead of xml
	
	
/magehealth/cims/unconsigned/

Available params:

	threshold	value in seconds
	output		pingdom / gecko_number / nagios / default =  tOm data structure
	json	    1 / 0 to return formatted as json instead of xml