OtherMedia_MageHealth
=====================

The OtherMedia_MageHealth module provides an interface for providing information to third parties regarding specific settings
in Magento, time since last order or other relevant information

Overview
------------

The OtherMedia MageHealth module aims offer the following features

* Supply information on current config values and compare to a test value (coming soon)
* Supply information on the time since last order and compare to a threshold value
* Supply information to be used by Pingdom, Geckoboard or Nagios
* Send notifications via Pushover.net or Jabber as required